#!/bin/bash

git submodule update --init --recursive

SECP256K1_SRC_DIR=${SECP256K1_SRC_DIR:-./aux/secp256k1}
CHECK_SRC_DIR=${CHECK_SRC_DIR:-./aux/check}

pushd $SECP256K1_SRC_DIR
autoreconf --install --force
./configure --enable-module-recovery
make
popd

pushd $CHECK_SRC_DIR
autoreconf --install --force
./configure
make
popd


