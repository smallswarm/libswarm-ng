#include <string.h>

#include "keccak-tiny.h"

#include "keystore.h"
#include "swarm.h"
#include "soc.h"

// z must be minimum 84 bytes long (32 bytes for out, 20 bytes for topic, 32 bytes for index)
int soc_identifier(unsigned char *z, const unsigned char *topic, const unsigned char *index) {
	int r;
	unsigned char *p;
	unsigned char *src;
	size_t src_sz;
	
	src_sz = SWARM_SOC_TOPIC_SIZE + SWARM_SOC_INDEX_SIZE;

	p = z + SWARM_WORD_SIZE;
	src = p;
	memcpy(p, topic, SWARM_SOC_TOPIC_SIZE);

	p += SWARM_SOC_TOPIC_SIZE;
	memcpy(p, index, SWARM_SOC_INDEX_SIZE);

	r = keccak_hash_btc(z, SWARM_WORD_SIZE, src, src_sz);
	if (r < 0) {
		return 1;
	}

	return 0;
}

// z must be minimum 84 bytes long (32 bytes for out, 20 bytes for topic, 32 bytes for index)
int soc_address(unsigned char *z, const unsigned char *identifier, const unsigned char *address) {
	int r;
	unsigned char *p;
	unsigned char *src;
	size_t src_sz;
	
	src_sz = SWARM_SOC_IDENTIFIER_SIZE + SWARM_ADDRESS_SIZE;

	p = z + SWARM_WORD_SIZE;
	src = p;
	memcpy(p, identifier, SWARM_SOC_IDENTIFIER_SIZE);

	p += SWARM_SOC_IDENTIFIER_SIZE;
	memcpy(p, address, SWARM_ADDRESS_SIZE);

	r = keccak_hash_btc(z, SWARM_WORD_SIZE, src, src_sz);
	if (r < 0) {
		return 1;
	}

	return 0;
}

unsigned char* soc_serialize(const soc_chunk_t *chunk, unsigned char *z, size_t *sz) {
	int crsr;

	crsr = 0;
	memcpy(z, chunk, SWARM_SOC_IDENTIFIER_SIZE + SWARM_SIGNATURE_SIZE);
	crsr += SWARM_SOC_IDENTIFIER_SIZE + SWARM_SIGNATURE_SIZE;

	chunk_serialize(&chunk->data, z + crsr, sz);

	*sz += crsr;

	return z;
}


// must be minimum 96 bytes long 
int soc_digest(const soc_chunk_t *chunk, unsigned char *z) {
	int r;
	int crsr;
	unsigned char *p;

	p = z + SWARM_WORD_SIZE;
	crsr = 0;
	memcpy(p + crsr, chunk->identifier, SWARM_SOC_IDENTIFIER_SIZE);
	crsr += SWARM_SOC_IDENTIFIER_SIZE;
	memcpy(p + crsr, chunk->data.hash, SWARM_WORD_SIZE);
	crsr += SWARM_WORD_SIZE;

	r = keccak_hash_btc(z, SWARM_WORD_SIZE, p, crsr);
	if (r < 0) {
		return 1;
	}

	return 0;
}

int soc_verify(const soc_chunk_t *chunk, const keystore_key_t *key_cmp) {
	int r;
	unsigned char b[96];
	keystore_key_t key;
	keystore_key_t *key_r;

	r = soc_digest(chunk, b);
	if (r != 0) {
		return 1;
	}

	key_r = key_recover(&key, chunk->signature, b);
	if (key_r == NULL) {
		return 1;
	}

	r = key_from_public(&key);
	if (r != 0) {
		return 1;
	}

	r = memcmp(key.label, key_cmp->label, SWARM_ADDRESS_SIZE);
	if (r != 0) {
		return 1;
	}

	return 0;
}
