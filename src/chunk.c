#include <stddef.h>
#include "string.h"

#include "chunk.h"
#ifdef LIBSWARM_MALLOC
#include "bmt_malloc.h"
#else
#include "bmt.h"
#endif

unsigned char* chunk_serialize(const swarm_chunk_t *chunk, unsigned char *z, size_t *sz) {
	int crsr;

	crsr = 0;

	memcpy(z, chunk->span, SWARM_DATA_LENGTH_TYPESIZE);
	crsr += SWARM_DATA_LENGTH_TYPESIZE;

	memcpy(z + crsr, chunk->payload, chunk->payload_sz);
	*sz = crsr + chunk->payload_sz;

	return z;
}

int chunk_verify(const swarm_chunk_t *chunk) {
	int r;
	bmt_t bctx;

	bmt_init(&bctx, chunk->payload, chunk->payload_sz, (bmt_spansize_t)*chunk->span);
	r = bmt_sum(&bctx);
	if (r != 0) {
		return 1;
	}	
	
	r = memcmp(chunk->hash, bctx.buf, SWARM_WORD_SIZE);
	if (r != 0) {
		return 1;
	}

	return 0;
}
