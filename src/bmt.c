#include <string.h>
#include <stdlib.h>

#include "keccak-tiny.h"
#include "endian.h"

#include "swarm.h"
#ifdef LIBSWARM_MALLOC
#include "bmt_malloc.h"
#else
#include "bmt.h"
#endif

#define _SWARM_DIGEST_INPUT_SIZE SWARM_WORD_SIZE*2
#define _SWARM_ROLLUP_TARGET SWARM_BLOCK_SIZE + SWARM_DATA_LENGTH_TYPESIZE + SWARM_WORD_SIZE

extern void dlog(int p, int v);

static int bmt_rollup(bmt_t *bctx) {
	unsigned char *last_target = bctx->w_ptr + SWARM_WORD_SIZE;
	unsigned char *start = bctx->w_ptr;
	unsigned char buf[256];
	int r;

	while (last_target != 0x00) {
		while (bctx->r_ptr < bctx->target) {
			r = keccak_hash_btc((unsigned char*)buf, SWARM_WORD_SIZE, (unsigned char*)bctx->r_ptr, _SWARM_DIGEST_INPUT_SIZE);
			if (r < 0) {
				return 1;
			}
			memcpy(bctx->w_ptr, buf, SWARM_WORD_SIZE);
			bctx->w_ptr += SWARM_WORD_SIZE;
			bctx->r_ptr += _SWARM_DIGEST_INPUT_SIZE;
		}
		bctx->target = start + ((bctx->target - start) / 2);
		if (bctx->target == last_target) {
			last_target = 0x00;
		}
		bctx->r_ptr = start;
		bctx->w_ptr = start;
	}

	r = keccak_hash_btc((unsigned char*)buf, SWARM_WORD_SIZE, (unsigned char*)bctx->buf, SWARM_DATA_LENGTH_TYPESIZE + SWARM_WORD_SIZE);
	if (r < 0) {
		return 1;
	}
	memcpy(bctx->buf, buf, SWARM_WORD_SIZE);
	return 0;
}

void bmt_init(bmt_t *bctx, const unsigned char *input, const size_t input_length, const bmt_spansize_t data_length) {
#ifdef LIBSWARM_MALLOC
	bctx->buf = malloc(sizeof(unsigned char) * SWARM_DATA_LENGTH_TYPESIZE + SWARM_BLOCK_SIZE);
#endif
	bctx->w_ptr = bctx->buf + SWARM_DATA_LENGTH_TYPESIZE;
	bctx->r_ptr = bctx->w_ptr;
	bctx->target = bctx->w_ptr + SWARM_BLOCK_SIZE;
	memset(bctx->buf, 0, SWARM_DATA_LENGTH_TYPESIZE + SWARM_BLOCK_SIZE);

	memcpy((unsigned char*)bctx->buf, &data_length, sizeof(bmt_spansize_t));
	to_endian(CONVERT_LITTLEENDIAN, SWARM_DATA_LENGTH_TYPESIZE, (unsigned char*)bctx->buf);

	memcpy(bctx->w_ptr, input, input_length);
}

int bmt_sum(bmt_t *bctx) {
	return bmt_rollup(bctx);
}


int bmt_hash(unsigned char *zOut, const unsigned char *input, const size_t input_length, const bmt_spansize_t data_length) {
	int i;
	int r;

	bmt_t bctx;
	bmt_init(&bctx,	input, input_length, data_length);
	r = bmt_sum(&bctx);
	memcpy(zOut, bctx.buf, SWARM_WORD_SIZE);
#ifdef LIBSWARM_MALLOC
	bmt_free(&bctx);
#endif
	return r;
}

#ifdef LIBSWARM_MALLOC
void bmt_free(bmt_t *bctx) {
	free(bctx->buf);
}
#endif
