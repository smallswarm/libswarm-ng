#ifndef LASH_HEX_H_
#define LASH_HEX_H_

int toHex(const unsigned char *data, size_t l, unsigned char *zHex, size_t *z);
int hexchr2bin(const char hex, char *out);
size_t hex2bin(const char *hex, unsigned char *out);

#endif // LASH_HEX_H_
