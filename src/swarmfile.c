#include <stddef.h>
#include <string.h>

#include "def.h"
#include "endian.h"
#include "swarmfile.h"

static inline void filehash_callback(filehash_t *fctx, const unsigned char *hash, const unsigned char *data, const size_t data_length, const bmt_spansize_t span_size) {
	if (fctx->callback != NULL) {
		if (span_size < 0) {
			(*fctx->callback)(hash, data, data_length + SWARM_DATA_LENGTH_TYPESIZE, fctx->callback_static);
		} else {
			unsigned char out_data[SWARM_BLOCK_SIZE + SWARM_DATA_LENGTH_TYPESIZE];
			memcpy(out_data, &span_size, SWARM_DATA_LENGTH_TYPESIZE);
			to_endian(CONVERT_LITTLEENDIAN, SWARM_DATA_LENGTH_TYPESIZE, (unsigned char*)out_data);
			memcpy(out_data + SWARM_DATA_LENGTH_TYPESIZE, data, data_length);
			(*fctx->callback)(hash, out_data, data_length + SWARM_DATA_LENGTH_TYPESIZE, fctx->callback_static);
		}
	}
}


void filehash_reset(filehash_t *fctx) {
	int i;
	for (i = 0; i < SWARM_LEVELS; i++) {
		fctx->ptr[i] = fctx->buf;
		fctx->writes[i] = 0;
	}
	fctx->length = 0;
}


static bmt_spansize_t filehash_finalize_level(filehash_t *fctx, int level) {
	bmt_t *bctx;
	int l;
	int r;
	int next_level;
	int blocks;
	int blocks_span_length;
	int remainder;

	bctx = &fctx->bmt_context;

	next_level = level + 1;

	if (fctx->ptr[level] == fctx->target) {
		return fctx->length;
	}

	l = fctx->ptr[level] - fctx->ptr[next_level];
	blocks = (l / SWARM_WORD_SIZE);

	remainder = fctx->length % fctx->spans[level];

	if (blocks == 1) {
		fctx->ptr[next_level] = fctx->ptr[level];
		return filehash_finalize_level(fctx, next_level);
	} else {
		blocks_span_length = (blocks - 1) * fctx->spans[level];
		if (remainder > 0) {
			blocks_span_length += remainder;
		} else {
			blocks_span_length += fctx->spans[level];
		}
		bmt_init(bctx, fctx->ptr[next_level], l, (bmt_spansize_t)(blocks_span_length));
		r = bmt_sum(bctx);
		if (r != 0) {
			return -1;
		}
		filehash_callback(fctx, bctx->buf, fctx->ptr[next_level], l, (bmt_spansize_t)(blocks_span_length));
		memcpy(fctx->ptr[next_level], bctx->buf, SWARM_WORD_SIZE);
		fctx->ptr[next_level] += SWARM_WORD_SIZE;
	}
	return filehash_finalize_level(fctx, next_level);
}


bmt_spansize_t filehash_sum(filehash_t *fctx) {
	return filehash_finalize_level(fctx, 0);
}


void filehash_init_callback(filehash_t *fctx, void (*callback)(const unsigned char*, const unsigned char*, const size_t, void*), void *callback_static) {
	int i;
	int l;

	l = SWARM_BLOCK_SIZE;

	for (i = 0; i < SWARM_LEVELS; i++) {
		fctx->spans[i] = l;
		l *= SWARM_BATCH_SIZE;
	}
	fctx->target = fctx->buf + SWARM_WORD_SIZE;
	fctx->callback = callback;
	fctx->callback_static = callback_static;

	filehash_reset(fctx);	
}

void filehash_init(filehash_t *fctx) {
	return filehash_init_callback(fctx, NULL, NULL);
}


static int filehash_write_hash(filehash_t *fctx, int level, const unsigned char *data) {
	bmt_t *bctx;
	int next_level;
	int r;

	fctx->writes[level] += 1;
	memcpy(fctx->ptr[level], data, SWARM_WORD_SIZE);
	if (fctx->writes[level] % SWARM_BATCH_SIZE == 0) {
		bctx = &fctx->bmt_context;
		next_level = level + 1;
		bmt_init(bctx, fctx->ptr[next_level], SWARM_BLOCK_SIZE, (long long)fctx->spans[next_level]);
		r = bmt_sum(bctx);
		if (r != 0) {
			return -1;
		}
		filehash_callback(fctx, bctx->buf, fctx->ptr[next_level], SWARM_BLOCK_SIZE, (bmt_spansize_t)fctx->spans[next_level]);
		r = filehash_write_hash(fctx, level + 1, bctx->buf);
		if (r != 0) {
			return -1;
		}
		fctx->ptr[level] = fctx->ptr[next_level];
	} else {
		fctx->ptr[level] += SWARM_WORD_SIZE;
	}
	return 0;
}

int filehash_write(filehash_t *fctx, const unsigned char *data, const size_t data_length) {
	bmt_t *bctx;
	int r;

	bctx = &fctx->bmt_context;

	bmt_init(bctx, data, data_length, (long long)data_length);
	r = bmt_sum(bctx);
	if (r != 0) {
		return -1;
	}
	filehash_callback(fctx, bctx->buf, data, data_length, (bmt_spansize_t)data_length);
	r = filehash_write_hash(fctx, 0, bctx->buf);
	if (r != 0) {
		return -1;
	}
	fctx->length += data_length;

	return data_length;
}


