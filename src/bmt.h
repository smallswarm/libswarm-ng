#ifndef _LIBSWARM_BMT_H
#define _LIBSWARM_BMT_H

#include "swarm.h"

typedef struct bmt {
	unsigned char buf[SWARM_DATA_LENGTH_TYPESIZE + SWARM_BLOCK_SIZE];
	unsigned char *w_ptr;
	unsigned char *r_ptr;
	unsigned char *target;
} bmt_t;	

#include "bmt_interface.h"

#endif // _LIBSWARM_BMT_H
