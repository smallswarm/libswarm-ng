#ifndef _LIBSWARM_FILE
#define _LIBSWARM_FILE

#ifdef LIBSWARM_MALLOC
#include "bmt_malloc.h"
#else
#include "bmt.h"
#endif

#define SWARM_BATCH_SIZE (int)(SWARM_BLOCK_SIZE / SWARM_WORD_SIZE)
#define SWARM_LEVELS 9

typedef struct filehash {
	unsigned char buf[SWARM_LEVELS * SWARM_BLOCK_SIZE];
	unsigned char *ptr[SWARM_LEVELS];
	unsigned char *target;
	long long writes[SWARM_LEVELS];
	long long spans[SWARM_LEVELS];
	long long length;
	void (*callback)(const unsigned char*, const unsigned char*, const size_t, void*);
	void *callback_static;
	bmt_t bmt_context;
} filehash_t;

void filehash_reset(filehash_t *filehash_context);
void filehash_init(filehash_t *filehash_context);
void filehash_init_callback(filehash_t *filehash_context, void (*callback)(const unsigned char*, const unsigned char*, const size_t, void*), void *callback_static);
int filehash_write(filehash_t *filehash_context, const unsigned char *data, const size_t data_length);
bmt_spansize_t filehash_sum(filehash_t *filehash_content);

#endif // _LIBSWARM_FILE
