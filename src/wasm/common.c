#include <stddef.h>

#define MALLOC_PAGE_SIZE 1024
#define STACK_OFFSET 1024

extern unsigned char __heap_base;

void *malloc(size_t c) {
	int i;
	int n;
	int offset;
	unsigned char *offset_p;

	unsigned char *ptr;
	unsigned char *base = (unsigned char*)(&__heap_base);

	offset = *((int*)(base)) + STACK_OFFSET;

	n = (c / MALLOC_PAGE_SIZE) + 1;
	n *= MALLOC_PAGE_SIZE;

	ptr = base + offset;
	
	offset += n;
	offset_p = (unsigned char*)&offset;
	for (i = 0; i < 4; i++) {
		*(base+i) = *(offset_p+i);
	}

	offset = *((int*)(base));

	return (void*)ptr;
}

void *memset(void *p, int v, size_t c) {
	int i;

	for (i = 0; i < c; i++) {
		*((char*)(p+i)) = *((char*)(v+i));
	}
	return p;
}

void *memcpy(void *dst, const void *src, size_t c) {
	int i ;

	for (i = 0; i < c; i++) {
		*((char*)(dst+i)) = *((char*)(src+i));
	}
	return dst;
}

void free(void *ptr) {
	return;
}
