#include <string.h>
#include <stdlib.h>

#include "bmt.h"

size_t keccak_hash_heap_init(size_t outLen, size_t inLen) {
	return (size_t)malloc(outLen + inLen);
}

void keccak_hash_heap_free(void *mem) {
	free(mem);
}

void *bmt_hash_heap(const unsigned char *input, const size_t input_length, const bmt_spansize_t data_length) {
	unsigned char *out;

	out = malloc(SWARM_WORD_SIZE + input_length);

	bmt_hash(out, input, input_length, data_length);

	return out;
}

void bmt_hash_free(void * out) {
	free(out);
}
