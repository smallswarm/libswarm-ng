#ifndef _LIBSWARM_SIGN_H
#define _LIBSWARM_SIGN_H

#include "swarm.h"

typedef struct keystore_key {
	unsigned char pk[SWARM_PRIVATE_KEY_SIZE];
	unsigned char pubk[SWARM_PUBLIC_KEY_SIZE];
	unsigned char label[SWARM_KEY_LABEL_SIZE];
} keystore_key_t;


typedef struct keystore {
	size_t pk_sz;
	size_t label_sz;
	size_t digest_sz;
	keystore_key_t *keys;
	size_t keys_count;
	keystore_key_t* (*label)(keystore_key_t *key);
} keystore_t;


keystore_key_t* keystore_put(keystore_t *keystore, const keystore_key_t *z, const unsigned char *passphrase, size_t passphrase_sz);
keystore_key_t* keystore_get(const keystore_t *keystore, keystore_key_t *z, const int idx);
unsigned char* keystore_sign(const keystore_t *keystore, unsigned char *z, const int key_idx, const unsigned char *digest);
keystore_key_t* key_recover(keystore_key_t *key, const unsigned char *signature, const unsigned char *digest);
int key_from_public(keystore_key_t *key);
int key_from_private(keystore_key_t *key);

#endif // _LIBSWARM_SIGN_H
