#ifndef _LIBSWARM_CHUNK_H
#define _LIBSWARM_CHUNK_H

#include "def.h"

typedef struct swarm_chunk {
	unsigned char hash[SWARM_WORD_SIZE];
	unsigned char span[SWARM_DATA_LENGTH_TYPESIZE];
	size_t payload_sz;
	unsigned char *payload;
} swarm_chunk_t;

unsigned char* chunk_serialize(const swarm_chunk_t *chunk, unsigned char *z, size_t *sz);
int chunk_verify(const swarm_chunk_t *chunk);
	
#endif // _LIBSWARM_CHUNK_H
