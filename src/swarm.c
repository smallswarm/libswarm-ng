#include <stdint.h>
#include <stddef.h>

#include "keccak-tiny.h"
#include "swarm.h"
#include "keystore.h"

int keccak_hash_btc(unsigned char *out, size_t out_sz, const unsigned char *in, size_t in_sz) {
	return keccak_hash((uint8_t*)out, out_sz, (uint8_t*)in, in_sz, SWARM_KECCAK_RATE, SWARM_KECCAK_PADDING);
}

//int keccak_hash_btc_v(unsigned char *out, size_t out_sz, const unsigned char *in, size_t in_sz, const unsigned char *vector, size_t *vector_sz, int vector_count) {
//	int i;
//	int sz;
//	int *p;
//
//	src = vector;
//	p = out + SWARM_WORD_SIZE;
//	for (i = 0; i < vector_count; i++) {
//		sz = *(vector_sz +i );
//		memcpy
//	}
//}
//
