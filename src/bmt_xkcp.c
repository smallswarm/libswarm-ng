#include <string.h>

#include <XKCP/KeccakHash.h>
#include <XKCP/KangarooTwelve.h>
#include <XKCP/SP800-185.h>
#include "endian.h"

#include "bmt.h"

#define _DIGEST_INPUT_SIZE _WORD_SIZE * 2
#define _ROLLUP_TARGET BLOCK_SIZE + _DATA_LENGTH_TYPESIZE + _WORD_SIZE


// TODO: broken, must be updated to use new pointers
static int bmt_rollup(bmt_t *bmt_content) {
	char *last_target = bmt_content->ptr + _WORD_SIZE;

	while (last_target != 0x00) {
		while (bmt_content->ptr != bmt_content->target) {
			Keccak_HashInstance instance;
			if (Keccak_HashInitialize(&instance, 1088, 512, 256, 0x01)) {
				return 1;
			}
			Keccak_HashUpdate(&instance, bmt_content->ptr, _DIGEST_INPUT_SIZE);
			Keccak_HashFinal(&instance, bmt_content->ptr);
			bmt_content->ptr += _DIGEST_INPUT_SIZE;
		}
		bmt_content->target = (bmt_content->target - bmt_content->ptr) / 2;
		if (bmt_content->target < last_target) {
			last_target = 0x00;
		}
	}

	Keccak_HashInstance instance;
	if (Keccak_HashInitialize(&instance, 1088, 512, 256, 0x01)) {
		return 1;
	}
	Keccak_HashUpdate(&instance, bmt_content->buf, _DATA_LENGTH_TYPESIZE + _WORD_SIZE);
	Keccak_HashFinal(&instance, bmt_content->buf);

	return 0;
}


void bmt_init(bmt_t *bmt_content, char *input, size_t input_length, long long data_length) {
	bmt_content->ptr = (char*)bmt_content->buf+_DATA_LENGTH_TYPESIZE;
	bmt_content->target = bmt_content->ptr + BLOCK_SIZE;
	memset(bmt_content->buf, 0, BLOCK_SIZE+_DATA_LENGTH_TYPESIZE);

	memcpy((char*)bmt_content->buf, &data_length, sizeof(long long));
	to_endian(CONVERT_BIGENDIAN, _DATA_LENGTH_TYPESIZE, bmt_content->buf);

	memcpy(bmt_content->ptr, input, input_length);
}


int bmt_sum(bmt_t *bmt_content) {
	return bmt_rollup(bmt_content);
}


