#ifndef _LIBSWARM_H
#define _LIBSWARM_H

#include "def.h"

int keccak_hash_btc(unsigned char *out, size_t out_sz, const unsigned char *in, size_t in_sz);
int keccak_hash_btc_v(unsigned char *out, size_t out_sz, const unsigned char *in, size_t in_sz, const unsigned char *vector, size_t vector_sz);

#endif // _LIBSWARM_H
