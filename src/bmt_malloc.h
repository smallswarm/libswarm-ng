#ifndef _LIBSWARM_BMT_MALLOC
#define _LIBSWARM_BMT_MALLOC

#include "swarm.h"

typedef struct bmt {
	unsigned char *buf;
	unsigned char *w_ptr;
	unsigned char *r_ptr;
	unsigned char *target;
} bmt_t;	

#include "bmt_interface.h"
void bmt_free(bmt_t *bctx);

#endif // _LIBSWARM_BMT
