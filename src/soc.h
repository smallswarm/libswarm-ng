#ifndef _LIBSWARM_SOC_H
#define _LIBSWARM_SOC_H

#include "def.h"
#include "chunk.h"
#include "keystore.h"

#define SWARM_SOC_TOPIC_SIZE 20
#define SWARM_SOC_INDEX_SIZE SWARM_WORD_SIZE
#define SWARM_SOC_IDENTIFIER_SIZE SWARM_WORD_SIZE

typedef struct soc_chunk {
	unsigned char identifier[SWARM_SOC_IDENTIFIER_SIZE];
	unsigned char signature[SWARM_SIGNATURE_SIZE];
	swarm_chunk_t data;
} soc_chunk_t;

int soc_identifier(unsigned char *z, const unsigned char *topic, const unsigned char *index);
int soc_address(unsigned char *z, const unsigned char *identifier, const unsigned char *address);
int soc_digest(const soc_chunk_t* chunk, unsigned char *z);
unsigned char* soc_serialize(const soc_chunk_t *chunk, unsigned char *z, size_t *sz);
int soc_verify(const soc_chunk_t *chunk, const keystore_key_t *key_cmp);

#endif // _LIBSWARM_SOC_H
