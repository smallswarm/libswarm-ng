#include <string.h>

#include "secp256k1.h"
#include "secp256k1_recovery.h"

#include "keystore.h"


unsigned char* keystore_sign(const keystore_t *keystore, unsigned char *z, const int key_idx, const unsigned char *digest) {
	int r;
	int recover;
	char recover_byte;
	secp256k1_context *ecctx;
	secp256k1_ecdsa_recoverable_signature signature;
	keystore_key_t key;

	ecctx = secp256k1_context_create(SECP256K1_CONTEXT_SIGN | SECP256K1_CONTEXT_VERIFY);
	
	keystore_get(keystore, &key, key_idx);
	r = secp256k1_ecdsa_sign_recoverable(ecctx, &signature, digest, key.pk, NULL, NULL);
	if (r == 0) {
		return NULL;
	}

	r = secp256k1_ecdsa_recoverable_signature_serialize_compact(ecctx, z, &recover, &signature);
	if (r == 0) {
		return NULL;
	}
	*(z+64) = (unsigned char)recover;

	return z;
}


// will _copy_ key to keystore
keystore_key_t* keystore_put(keystore_t *keystore, const keystore_key_t *key_in, const unsigned char *passphrase, size_t passphrase_sz) {
	keystore_key_t *key_out;

	key_out = keystore->keys + keystore->keys_count;

	memcpy(key_out, key_in, sizeof(keystore_key_t));

	keystore->keys_count++;	

	return key_out;
}


keystore_key_t* keystore_get(const keystore_t *keystore, keystore_key_t *z, const int idx) {
	memcpy(z, keystore->keys+idx, sizeof(keystore_key_t));
	return z;
}


int key_from_public(keystore_key_t *key) {
	int r;
	secp256k1_context *ecctx;
	unsigned char digest[SWARM_WORD_SIZE];

	r = keccak_hash_btc(digest, SWARM_WORD_SIZE, key->pubk + 1, SWARM_PUBLIC_KEY_SIZE - 1);
	if (r < 0) {
		return 1;
	}

	memcpy(key->label, digest + SWARM_WORD_SIZE - SWARM_ADDRESS_SIZE, SWARM_ADDRESS_SIZE);

	return 0;
}


int key_from_private(keystore_key_t *key) {
	int r;
	secp256k1_context *ecctx;
	secp256k1_pubkey public_key_o;
	size_t public_key_sz;
	unsigned char *p;

	ecctx = secp256k1_context_create(SECP256K1_CONTEXT_SIGN);

	r = secp256k1_ec_pubkey_create(ecctx, &public_key_o, key->pk);
	if (r == 0) {
		return 1;
	}

	public_key_sz = SWARM_PUBLIC_KEY_SIZE;
	r = secp256k1_ec_pubkey_serialize(ecctx, key->pubk, &public_key_sz, &public_key_o, SECP256K1_EC_UNCOMPRESSED);
	if (r == 0) {
		return 1;
	}

	r = key_from_public(key);
	if (r != 0) {
		return 1;
	}

	return 0;
}


int key_recover_compact(const unsigned char *signature, unsigned char *public_key, const unsigned char *digest) {
	int r;
	size_t public_key_sz;

	secp256k1_context *ecctx;
	secp256k1_ecdsa_recoverable_signature signature_o;
	secp256k1_pubkey public_key_o;

	ecctx = secp256k1_context_create(SECP256K1_CONTEXT_NONE);

	r = secp256k1_ecdsa_recoverable_signature_parse_compact(ecctx, &signature_o, signature, (int)*(signature+64));
	if (r == 0) {
		return 1;
	}

	r = secp256k1_ecdsa_recover(ecctx, &public_key_o, &signature_o, digest);
	if (r == 0) {
		return 1;
	}

	public_key_sz = SWARM_PUBLIC_KEY_SIZE;
	r = secp256k1_ec_pubkey_serialize(ecctx, public_key, &public_key_sz, &public_key_o, SECP256K1_EC_UNCOMPRESSED);
	if (r == 0) {
		return 1;
	}
	if (public_key_sz != SWARM_PUBLIC_KEY_SIZE) {
		return 1;
	}

	return 0;
}


keystore_key_t* key_recover(keystore_key_t *z, const unsigned char *signature, const unsigned char *digest) {
	int r;

	r = key_recover_compact(signature, z->pubk, digest);
	if (r != 0) {
		return NULL;
	}

	return z;
}
