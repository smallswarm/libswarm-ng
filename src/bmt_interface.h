typedef long long bmt_spansize_t;

void bmt_init(bmt_t *bmt_context, const unsigned char *input, const size_t input_length, const bmt_spansize_t data_length);
int bmt_sum(bmt_t *bmt_context);
int bmt_hash(unsigned char *zOut, const unsigned char *input, const size_t input_length, const bmt_spansize_t data_length);


