#include <check.h>
#include <stdlib.h>

#include "soc.h"
#include "hex.h"
#include "common.h"
#include "def.h"
#include "keystore.h"


START_TEST(check_soc_identifier) {
	int i;
	int r; 
	char out[84];
	char in[52];

	for (i = 0; i < 52; i++) {
		in[i] = i;
	}

	r = soc_identifier(out, in, in+20);
	ck_assert_int_eq(r, 0);
}
END_TEST


START_TEST(check_soc_address) {
	int i;
	int r; 
	char out[84];
	char in[52];

	for (i = 51; i >= 0; i--) {
		in[i] = i;
	}

	r = soc_identifier(out, in, in+32);
	ck_assert_int_eq(r, 0);
}
END_TEST


START_TEST(check_soc_digest) {
	soc_chunk_t chunk;
	int i;
	int r; 
	char out[96];

	r = soc_digest(&chunk, out);
	ck_assert_int_eq(r, 0);
}
END_TEST


START_TEST(check_soc_verify) {
	int r;
	soc_chunk_t chunk;
	keystore_key_t key;
	keystore_key_t *key_returned;
	keystore_t keystore;
	keystore_t *keystore_returned;
	unsigned char digest[96];

	keystore_returned = keystore_init(&keystore);
	ck_assert_ptr_nonnull(keystore_returned);

	hex2bin(TEST_PRIVATE_KEY, key.pk);

	key_returned = keystore_put(&keystore, &key, NULL, 0);
	ck_assert_ptr_nonnull(key_returned);

	r = key_from_private(&key);
	ck_assert_int_eq(r, 0);

	r = soc_digest(&chunk, digest);
	ck_assert_int_eq(r, 0);

	keystore_sign(&keystore, chunk.signature, 0, digest);
		
	r = soc_verify(&chunk, &key);
	ck_assert_int_eq(r, 0);

	keystore_free(&keystore);
}
END_TEST


START_TEST(check_soc_serialize) {
	soc_chunk_t soc_chunk;
	long long span;
	unsigned char wire[4096*2];
	unsigned char wire_out[4096*2];
	size_t wire_length;
	unsigned char *wire_returned;
	struct block_generator bg;
	int crsr;

	bg.v = 0;
	bg.m = 256;

	block_generate(&bg, (char*)soc_chunk.identifier, SWARM_SOC_IDENTIFIER_SIZE);
	block_generate(&bg, (char*)soc_chunk.signature, SWARM_SIGNATURE_SIZE);

	block_generate(&bg, soc_chunk.data.hash, SWARM_WORD_SIZE);
	soc_chunk.data.payload_sz = 1324;
	span = 2435;
	memcpy(&soc_chunk.data.span, &span, SWARM_DATA_LENGTH_TYPESIZE);
	block_generate(&bg, (char*)wire, soc_chunk.data.payload_sz);
	soc_chunk.data.payload = wire;
	memcpy(wire_out, wire, soc_chunk.data.payload_sz);

	wire_returned = soc_serialize(&soc_chunk, wire_out, &wire_length);

	crsr = 0;
	ck_assert_mem_eq(&soc_chunk, wire_returned + crsr, SWARM_SOC_IDENTIFIER_SIZE + SWARM_SIGNATURE_SIZE);
	crsr += SWARM_SOC_IDENTIFIER_SIZE + SWARM_SIGNATURE_SIZE;
	ck_assert_mem_eq(soc_chunk.data.span, wire_returned + crsr, SWARM_DATA_LENGTH_TYPESIZE);
	crsr += SWARM_DATA_LENGTH_TYPESIZE;
	ck_assert_mem_eq(soc_chunk.data.payload, wire_returned + crsr, soc_chunk.data.payload_sz);
	crsr += soc_chunk.data.payload_sz;

	ck_assert_int_eq(crsr, wire_length);
}
END_TEST

Suite * common_suite(void) {
	Suite *s;
	TCase *tc;

	s = suite_create("soc");
	tc = tcase_create("core");
	tcase_add_test(tc, check_soc_identifier);
	tcase_add_test(tc, check_soc_address);
	tcase_add_test(tc, check_soc_digest);
	tcase_add_test(tc, check_soc_verify);
	tcase_add_test(tc, check_soc_serialize);
	suite_add_tcase(s, tc);

	return s;
}

int main(void) {
	int n_fail;

	Suite *s;
	SRunner *sr;

	s = common_suite();	
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	n_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
