#include <check.h>
#include <stdlib.h>

#include "bmt_malloc.h"
#include "hex.h"
#include "common.h"


START_TEST(check_bmt_init) {
	bmt_t bmt_context;
	unsigned char *input = "foo";
	unsigned char input_length = 3;
	unsigned char data_length_bytes[] = {0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	int r;
	
	bmt_init(&bmt_context, input, input_length, 3);
	ck_assert_mem_eq(bmt_context.buf, data_length_bytes, sizeof(long long));
	ck_assert_mem_eq(bmt_context.w_ptr, input, 3);
	bmt_free(&bmt_context);
}
END_TEST


START_TEST(check_bmt_sum_foo) {
	bmt_t bmt_context;
	unsigned char *input = "foo";
	unsigned char v_chk[SWARM_WORD_SIZE];
	unsigned char input_length = 3;
	int r;

	bmt_init(&bmt_context, input, input_length, 3);
	r = bmt_sum(&bmt_context);
	ck_assert_int_eq(r, 0);

	hex2bin(HASH_OF_FOO, v_chk);
	ck_assert_mem_eq(bmt_context.buf, v_chk, SWARM_WORD_SIZE);
	bmt_free(&bmt_context);
}
END_TEST


START_TEST(check_bmt_sum_vector) {
	bmt_t bmt_context;
	int r;
	int i;

	int lengths[] = {
		SWARM_WORD_SIZE - 1,
		SWARM_WORD_SIZE,
		SWARM_WORD_SIZE + 1,
		SWARM_WORD_SIZE * 2 - 1,
		SWARM_WORD_SIZE * 2,
		SWARM_WORD_SIZE * 2 + 1,
		//SWARM_BLOCK_SIZE - 1,
		SWARM_BLOCK_SIZE,
	};
	unsigned char *vectors[] = {
		"ece86edb20669cc60d142789d464d57bdf5e33cb789d443f608cbd81cfa5697d",
		"0be77f0bb7abc9cd0abed640ee29849a3072ccfd1020019fe03658c38f087e02",
		"3463b46d4f9d5bfcbf9a23224d635e51896c1daef7d225b86679db17c5fd868e",
		"95510c2ff18276ed94be2160aed4e69c9116573b6f69faaeed1b426fea6a3db8",
		"490072cc55b8ad381335ff882ac51303cc069cbcb8d8d3f7aa152d9c617829fe",
		"541552bae05e9a63a6cb561f69edf36ffe073e441667dbf7a0e9a3864bb744ea",
		//"",
		"c10090961e7682a10890c334d759a28426647141213abda93b096b892824d2ef",
	};
	unsigned char v_chk[SWARM_WORD_SIZE];
	unsigned char buf[SWARM_BLOCK_SIZE];
	struct block_generator bg;

	bg.m = 255;

	for (i = 0; i < sizeof(vectors)/sizeof(vectors[0]); i++) {
		bg.v = 0;

		r = block_generate(&bg, buf, lengths[i]);
		ck_assert_int_eq(r, lengths[i]);

		bmt_init(&bmt_context, buf, lengths[i], lengths[i]);
		r = bmt_sum(&bmt_context);
		ck_assert_int_eq(r, 0);

		hex2bin(vectors[i], v_chk);
		ck_assert_mem_eq(bmt_context.buf, v_chk, SWARM_WORD_SIZE);
	}
	bmt_free(&bmt_context);
}
END_TEST


Suite * common_suite(void) {
	Suite *s;
	TCase *tc;

	s = suite_create("bmt");
	tc = tcase_create("core");
	tcase_add_test(tc, check_bmt_init);
	//tcase_add_test(tc, check_bmt_sum_foo);
	tcase_add_test(tc, check_bmt_sum_vector);
	suite_add_tcase(s, tc);

	return s;
}

int main(void) {
	int n_fail;

	Suite *s;
	SRunner *sr;

	s = common_suite();	
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	n_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
