#include <check.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#include "hex.h"
#include "swarmfile.h"

#include "common.h"


const char *callback_static_foo = "foo";
int file_callback_serial = 0;
int file_callback_dir_fd = -1;

void file_callback(const unsigned char *hash, const unsigned char *data, const size_t data_length, void *callback_static) {
	int fd;
	unsigned char filename[32];

	sprintf(filename, "d%d", file_callback_serial);
	fd = openat(file_callback_dir_fd, filename, O_CREAT | O_EXCL | O_RDWR | O_CLOEXEC | O_SYNC);
	ck_assert_int_ge(fd, 2);
	write(fd, data, data_length);
	close(fd);

	sprintf(filename, "h%d", file_callback_serial);
	fd = openat(file_callback_dir_fd, filename, O_CREAT | O_EXCL | O_RDWR | O_CLOEXEC | O_SYNC);
	ck_assert_int_ge(fd, 2);
	write(fd, hash, SWARM_WORD_SIZE);
	close(fd);

	ck_assert_str_eq((unsigned char*)callback_static, callback_static_foo);
	file_callback_serial++;
}


START_TEST(check_file_init) {
	ck_assert_int_eq(SWARM_BATCH_SIZE, 128);

	filehash_t fh;
	int r;

	filehash_init(&fh);

	ck_assert_ptr_eq(fh.ptr[SWARM_LEVELS-1], fh.buf);
	ck_assert_int_eq(fh.spans[1], SWARM_BATCH_SIZE * SWARM_BLOCK_SIZE);
}
END_TEST

START_TEST(check_file_single_write) {
	filehash_t fh;
	int r;

	const unsigned char *data = "foo";

	filehash_init(&fh);

	r = filehash_write(&fh, data, strlen(data));
	ck_assert_int_eq(r, 3);
	ck_assert_int_eq(fh.writes[0], 1);
	ck_assert_int_eq(fh.length, 3);
}
END_TEST


START_TEST(check_file_write_batch) {
	filehash_t fh;
	int r;
	int i;
	int j;
	int v;
	struct block_generator bg;

	bg.v = 0;
	bg.m = 255;

	char buf[SWARM_BLOCK_SIZE];

	filehash_init(&fh);

	for (i = 0; i < SWARM_BATCH_SIZE; i++) {
		r = block_generate(&bg, buf, SWARM_BLOCK_SIZE);
		ck_assert_int_eq(r, SWARM_BLOCK_SIZE);
		r = filehash_write(&fh, buf, SWARM_BLOCK_SIZE);
		ck_assert_int_eq(r, SWARM_BLOCK_SIZE);
	}
	ck_assert_int_eq(fh.writes[0], SWARM_BATCH_SIZE);
	ck_assert_int_eq(fh.length, SWARM_BATCH_SIZE * SWARM_BLOCK_SIZE);
	ck_assert_ptr_eq(fh.ptr[0], fh.ptr[1]);
	ck_assert_ptr_eq(fh.ptr[1], fh.buf + SWARM_WORD_SIZE);
}
END_TEST

START_TEST(check_file_callback_data) {
	filehash_t fh;
	int r;

	const unsigned char *data = "foo";
	unsigned char cbdir_template[] = "swarmfile_test_callback_data_XXXXXX";
	unsigned char *cbdir;

	cbdir = mkdtemp(cbdir_template);
	ck_assert_ptr_nonnull(cbdir);
	
	file_callback_serial = 0;
	file_callback_dir_fd = open(cbdir, O_DIRECTORY | O_SYNC);

	filehash_init_callback(&fh, file_callback, "foo");

	r = filehash_write(&fh, data, strlen(data));
	ck_assert_int_eq(r, 3);
	ck_assert_int_eq(fh.writes[0], 1);
	ck_assert_int_eq(fh.length, 3);
	close(file_callback_dir_fd);

}
END_TEST

START_TEST(check_file_callback_intermediate) {
	filehash_t fh;
	int i;
	int r;
	struct block_generator bg;

	unsigned char buf[SWARM_BLOCK_SIZE * (SWARM_BATCH_SIZE + 1)];
	unsigned char cbdir_template[] = "swarmfile_test_callback_intermediate_XXXXXX";
	unsigned char *cbdir;
	unsigned char cbfilename[1024];
	int cbfd;
	unsigned char cbbuf[SWARM_BLOCK_SIZE];

	file_callback_serial = 0;
	cbdir = mkdtemp(cbdir_template);
	ck_assert_ptr_nonnull(cbdir);
	
	file_callback_dir_fd = open(cbdir, O_DIRECTORY | O_SYNC);
	
	filehash_init_callback(&fh, file_callback, (char*)callback_static_foo);

	bg.v = 0;
	bg.m = 255;

	r = block_generate(&bg, buf, SWARM_BLOCK_SIZE * (SWARM_BATCH_SIZE + 1));
	ck_assert_int_eq(r, SWARM_BLOCK_SIZE * (SWARM_BATCH_SIZE + 1));
	for (i = 0; i < SWARM_BATCH_SIZE + 1; i++) {
		r = filehash_write(&fh, buf + i * SWARM_BLOCK_SIZE, SWARM_BLOCK_SIZE);
		ck_assert_int_eq(r, SWARM_BLOCK_SIZE);

		if (i > 127) {
			continue;
		}

		sprintf(cbfilename, "%s/d%d", cbdir, i);
		cbfd = open(cbfilename, O_RDONLY);
		ck_assert_int_gt(cbfd, 2);

		r = read(cbfd, cbbuf, SWARM_BLOCK_SIZE);
		ck_assert_int_eq(r, SWARM_BLOCK_SIZE);
		ck_assert_mem_eq(cbbuf, buf + i * SWARM_BLOCK_SIZE, SWARM_BLOCK_SIZE);
		close(cbfd);
	}

	sprintf(cbfilename, "%s/h128", cbdir);
	//cbfd = openat(file_callback_dir_fd, cbfilename, O_RDONLY);
	cbfd = open(cbfilename, O_RDONLY);
	ck_assert_int_gt(cbfd, 2);

	r = read(cbfd, cbbuf, SWARM_WORD_SIZE);
	ck_assert_int_eq(r, SWARM_WORD_SIZE);
	ck_assert_mem_eq(cbbuf, fh.buf, SWARM_WORD_SIZE);
	close(cbfd);

	r = filehash_sum(&fh);
	ck_assert_int_eq(r, SWARM_BLOCK_SIZE * (SWARM_BATCH_SIZE + 1));
	sprintf(cbfilename, "%s/h130", cbdir);
	cbfd = open(cbfilename, O_RDONLY);
	ck_assert_int_gt(cbfd, 2);

	r = read(cbfd, cbbuf, SWARM_WORD_SIZE);
	ck_assert_mem_eq(cbbuf, fh.buf, SWARM_WORD_SIZE);

	r = hex2bin("b8e1804e37a064d28d161ab5f256cc482b1423d5cd0a6b30fde7b0f51ece9199", cbbuf + SWARM_WORD_SIZE);
	ck_assert_int_eq(r, SWARM_WORD_SIZE);
	ck_assert_mem_eq(cbbuf, cbbuf + SWARM_WORD_SIZE, SWARM_WORD_SIZE);
	close(cbfd);

	close(file_callback_dir_fd);
	file_callback_dir_fd = -1;
}
END_TEST

START_TEST(check_file_vectors) {
	filehash_t fh;
	int r;
	int i;
	int j;
	int l;
	int whole;
	int part;
	int writes;
	unsigned char v_chk[SWARM_WORD_SIZE];
	struct block_generator bg;
	unsigned char buf[SWARM_BLOCK_SIZE];

	int lengths[] = {
		SWARM_BLOCK_SIZE,
		SWARM_BLOCK_SIZE + SWARM_WORD_SIZE - 1,
		SWARM_BLOCK_SIZE + SWARM_WORD_SIZE,
		SWARM_BLOCK_SIZE + (SWARM_WORD_SIZE * 2) - 1,
		SWARM_BLOCK_SIZE + (SWARM_WORD_SIZE * 2),
		SWARM_BLOCK_SIZE * 2,
		SWARM_BLOCK_SIZE * SWARM_BATCH_SIZE,
		SWARM_BLOCK_SIZE * SWARM_BATCH_SIZE + SWARM_WORD_SIZE - 1,
		SWARM_BLOCK_SIZE * SWARM_BATCH_SIZE + SWARM_WORD_SIZE,
		SWARM_BLOCK_SIZE * SWARM_BATCH_SIZE + (SWARM_WORD_SIZE * 2),
		SWARM_BLOCK_SIZE * (SWARM_BATCH_SIZE + 1),
		SWARM_BLOCK_SIZE * (SWARM_BATCH_SIZE + 2),
		SWARM_BLOCK_SIZE * SWARM_BATCH_SIZE * SWARM_BATCH_SIZE,
	};

	unsigned char *vectors[] = {
		"c10090961e7682a10890c334d759a28426647141213abda93b096b892824d2ef",
		"91699c83ed93a1f87e326a29ccd8cc775323f9e7260035a5f014c975c5f3cd28",
		"73759673a52c1f1707cbb61337645f4fcbd209cdc53d7e2cedaaa9f44df61285",
		"db1313a727ffc184ae52a70012fbbf7235f551b9f2d2da04bf476abe42a3cb42",
		"ade7af36ac0c7297dc1c11fd7b46981b629c6077bce75300f85b02a6153f161b",
		"29a5fb121ce96194ba8b7b823a1f9c6af87e1791f824940a53b5a7efe3f790d9",
		"3047d841077898c26bbe6be652a2ec590a5d9bd7cd45d290ea42511b48753c09",
		"e5c76afa931e33ac94bce2e754b1bb6407d07f738f67856783d93934ca8fc576",
		"485a526fc74c8a344c43a4545a5987d17af9ab401c0ef1ef63aefcc5c2c086df",
		"624b2abb7aefc0978f891b2a56b665513480e5dc195b4a66cd8def074a6d2e94",
		"b8e1804e37a064d28d161ab5f256cc482b1423d5cd0a6b30fde7b0f51ece9199",
		"59de730bf6c67a941f3b2ffa2f920acfaa1713695ad5deea12b4a121e5f23fa1",
		"522194562123473dcfd7a457b18ee7dee8b7db70ed3cfa2b73f348a992fdfd3b",
	};

	filehash_init(&fh);

	bg.m = 255;

	for (i = 0; i < sizeof(vectors)/sizeof(vectors[0]); i++) {
		bg.v = 0;

		filehash_reset(&fh);

		whole = lengths[i] / SWARM_BLOCK_SIZE;
		part = lengths[i] % SWARM_BLOCK_SIZE;
		writes = whole;
		if (part > 0) {
			writes++;
		}

		for (int j = 0; j < writes; j++) {
			if (j < whole) {
				l = SWARM_BLOCK_SIZE;
			} else {
				l = part;
			}
			r = block_generate(&bg, buf, l);
			ck_assert_int_eq(r, l);
			r = filehash_write(&fh, buf, l);
			ck_assert_int_eq(r, l);
		}

		r = filehash_sum(&fh);
		ck_assert_int_eq(r, lengths[i]);

		r = hex2bin(vectors[i], v_chk);
		ck_assert_mem_eq(fh.buf, v_chk, SWARM_WORD_SIZE);
	}
}
END_TEST




Suite * common_suite(void) {
	Suite *s;
	TCase *tch;
	TCase *tcc;
	TCase *tcv;

	s = suite_create("file");
	tch = tcase_create("hasher");
	tcc = tcase_create("callback");
	tcv = tcase_create("vectors");
	tcase_add_test(tch, check_file_init);
	tcase_add_test(tch, check_file_single_write);
	tcase_add_test(tch, check_file_write_batch);
	suite_add_tcase(s, tch);
	tcase_add_test(tcc, check_file_callback_data);
	tcase_add_test(tcc, check_file_callback_intermediate);
	suite_add_tcase(s, tcc);
	tcase_add_test(tcv, check_file_vectors);
	suite_add_tcase(s, tcv);

	return s;
}

int main(void) {
	int n_fail;

	Suite *s;
	SRunner *sr;

	s = common_suite();	
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	n_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
