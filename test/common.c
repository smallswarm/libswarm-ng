#include <stdlib.h>
#include <stddef.h>

#include "secp256k1.h"
#include "secp256k1_recovery.h"

#include "swarm.h"
#include "keystore.h"
#include "common.h"

int block_generate(struct block_generator *bg, char *buf, size_t l) {
	int i;

	for (i = 0; i < l; i++) {
		*(buf+i) = bg->v;
		bg->v++;
		bg->v %= bg->m;
	}

	return i;
}

keystore_t* keystore_init(keystore_t *keystore) {
	struct keystore_backend *backend;

	keystore->keys = malloc(sizeof(keystore_key_t) * 32);
	keystore->pk_sz = SWARM_PRIVATE_KEY_SIZE;
	keystore->label_sz = SWARM_KEY_LABEL_SIZE;
	keystore->digest_sz = SWARM_WORD_SIZE;
	keystore->keys_count = 0;

	return keystore;
}

void keystore_free(keystore_t *keystore) {
	free(keystore->keys);	
}

