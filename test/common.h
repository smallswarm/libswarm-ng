#include <stddef.h>

#include "keystore.h"

#define HASH_OF_FOO "2387e8e7d8a48c2a9339c97c1dc3461a9a7aa07e994c5cb8b38fd7c1b3e6ea48"
#define TEST_PRIVATE_KEY "5087503f0a9cc35b38665955eb830c63f778453dd11b8fa5bd04bc41fd2cc6d6"
#define TEST_PUBLIC_KEY "049f6bb6a7e3f5b7ee71756a891233d1415658f8712bac740282e083dc9240f5368bdb3b256a5bf40a8f7f9753414cb447ee3f796c5f30f7eb40a7f5018fc7f02e"
#define TEST_ADDRESS "eb3907ecad74a0013c259d5874ae7f22dcbcc95c"

// mockdata
struct block_generator {
	int v;
	int m;
};

int block_generate(struct block_generator *bg, char *buf, size_t l);

keystore_t* keystore_init(keystore_t *keystore);
void keystore_free(keystore_t *keystore);
