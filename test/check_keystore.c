#include <check.h>
#include <stdlib.h>

#include "keystore.h"
#include "hex.h"
#include "common.h"
#include "swarm.h"


START_TEST(check_keystore_init) {
	keystore_t keystore;

	keystore_init(&keystore);
}
END_TEST

START_TEST(check_keystore_getput) {
	keystore_t keystore;
	keystore_key_t key_one;
	keystore_key_t key_two;
	keystore_key_t key_out;
	keystore_key_t *key_returned;
	struct block_generator bg;

	bg.v = 0;
	bg.m = 256;
	block_generate(&bg, key_one.pk, 32);
	block_generate(&bg, key_two.pk, 32);

	keystore_init(&keystore);

	keystore_put(&keystore, &key_one, NULL, 0);
	keystore_put(&keystore, &key_two, NULL, 0);

	ck_assert_int_eq(keystore.keys_count, 2);

	key_returned = keystore_get(&keystore, &key_out, 1);

	ck_assert_mem_eq(&key_two, &key_out, sizeof(keystore_key_t));
	ck_assert_mem_eq(&key_two, key_returned, sizeof(keystore_key_t));

	keystore_free(&keystore);

}
END_TEST

START_TEST(check_keystore_key_generate) {
	int r;
	keystore_key_t key;

	unsigned char public_key_check[SWARM_PUBLIC_KEY_SIZE];
	unsigned char label_check[SWARM_ADDRESS_SIZE];

	hex2bin(TEST_PRIVATE_KEY, key.pk);
	r = key_from_private(&key);
	ck_assert_int_eq(r, 0);

	hex2bin(TEST_PUBLIC_KEY, public_key_check);
	ck_assert_mem_eq(key.pubk, public_key_check, SWARM_PUBLIC_KEY_SIZE);

	hex2bin(TEST_ADDRESS, label_check);
	ck_assert_mem_eq(key.label, label_check, SWARM_ADDRESS_SIZE);
}
END_TEST

START_TEST(check_keystore_sign) {
	int r;
	keystore_t keystore;
	keystore_key_t key;
	keystore_key_t key_recovered;
	keystore_key_t *key_recovered_returned;
	struct block_generator bg;
	unsigned char signature[SWARM_SIGNATURE_SIZE];
	unsigned char *signature_returned;
	unsigned char digest[SWARM_WORD_SIZE];
	unsigned char public_key[SWARM_PUBLIC_KEY_SIZE];
	unsigned char public_key_check[SWARM_PUBLIC_KEY_SIZE];

	keystore_init(&keystore);

	hex2bin(TEST_PRIVATE_KEY, key.pk);
	keystore_put(&keystore, &key, NULL, 0);

	hex2bin(HASH_OF_FOO, digest);

	signature_returned = keystore_sign(&keystore, signature, 0, digest);

	ck_assert_mem_eq(signature, signature_returned, sizeof(keystore_key_t));

	key_recovered_returned = key_recover(&key_recovered, signature, digest);
	ck_assert_ptr_nonnull(key_recovered_returned);

	hex2bin(TEST_PUBLIC_KEY, public_key_check);
	ck_assert_mem_eq(key_recovered.pubk, public_key_check, SWARM_PUBLIC_KEY_SIZE);
	ck_assert_mem_eq(key_recovered.pubk, key_recovered_returned->pubk, SWARM_PUBLIC_KEY_SIZE);

	keystore_free(&keystore);
}
END_TEST

Suite * common_suite(void) {
	Suite *s;
	TCase *tc;

	s = suite_create("keystore");
	tc = tcase_create("core");
	tcase_add_test(tc, check_keystore_init);
	tcase_add_test(tc, check_keystore_getput);
	tcase_add_test(tc, check_keystore_key_generate);
	tcase_add_test(tc, check_keystore_sign);
	suite_add_tcase(s, tc);

	return s;
}

int main(void) {
	int n_fail;

	Suite *s;
	SRunner *sr;

	s = common_suite();	
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	n_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
