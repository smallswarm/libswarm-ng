#include <check.h>
#include <stdlib.h>

#include "common.h"
#include "hex.h"
#include "def.h"
#include "chunk.h"
#include "bmt_malloc.h"


START_TEST(check_chunk_verify) {
	int r;
	swarm_chunk_t chunk;

	hex2bin(HASH_OF_FOO, chunk.hash);
	memset(chunk.span, 0, SWARM_DATA_LENGTH_TYPESIZE);
	chunk.span[0] = 3;
	chunk.payload = "foo";
	chunk.payload_sz = 3;

	r = chunk_verify(&chunk);
	ck_assert_int_eq(r, 0);

	chunk.span[0] = 4;
	r = chunk_verify(&chunk);
	ck_assert_int_eq(r, 1);
}
END_TEST


Suite * common_suite(void) {
	Suite *s;
	TCase *tc;

	s = suite_create("chunk");
	tc = tcase_create("core");
	tcase_add_test(tc, check_chunk_verify);
	suite_add_tcase(s, tc);

	return s;
}

int main(void) {
	int n_fail;

	Suite *s;
	SRunner *sr;

	s = common_suite();	
	sr = srunner_create(s);

	srunner_run_all(sr, CK_VERBOSE);
	n_fail = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
