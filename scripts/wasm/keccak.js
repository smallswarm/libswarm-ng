const fs = require('fs');

const memory = new WebAssembly.Memory({initial: 2});
const table = new WebAssembly.Table({initial: 3, element: 'anyfunc'});
const importsObj = {
	env: {
		memory: memory,
		__linear_memory: memory,
		__indirect_function_table: table,
	},
}

async function init() {
	const code_keccak = fs.readFileSync('./lib/keccak.wasm');
	const m_keccak = new WebAssembly.Module(code_keccak);
	const i_keccak = new WebAssembly.Instance(m_keccak, importsObj);

	let r;
	const p = i_keccak.exports.keccak_hash_heap_init(32, 3);
	console.debug('r ' + p);

	let outBuf = new Uint8Array(memory.buffer, p, 256);
	let inBuf = new Uint8Array(memory.buffer, p + 256, 3);
	inBuf.set([0x66, 0x6f, 0x6f], 0);
	r = i_keccak.exports.keccak_hash(p, 32, p+256, 3, 200-64, 1);
	if (r < 0) {
		console.error('error ' + r);
	} else {
		console.log('in ' + Buffer.from(new Uint8Array(memory.buffer, p+256, 3)).toString('hex'));
		console.log('out ' + Buffer.from(new Uint8Array(memory.buffer, p, 32)).toString('hex'));
	}
	i_keccak.exports.keccak_hash_heap_free(p);
}

init();
