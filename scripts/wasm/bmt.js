const fs = require('fs');

const memory = new WebAssembly.Memory({initial: 3});
const table = new WebAssembly.Table({initial: 10, element: 'anyfunc'});
const importsObj = {
	env: {
		memory: memory,
		dlog: function(prefix, l, value) {
			const v = new Uint8Array(memory.buffer, prefix, l);
			const s = new TextDecoder().decode(v);
			console.log('log: ' +  s, value);
		},
	}
}

async function init() {
	const code = fs.readFileSync('./lib/swarm.wasm');
	const m = new WebAssembly.Module(code);
	const i = new WebAssembly.Instance(m, importsObj);

	let r;
	let data = new Uint8Array(memory.buffer, 65535*2+1024, 3);
	data.set([0x66, 0x6f, 0x6f], 0);
	r = i.exports.bmt_hash_heap(65535*2+1024, 3, BigInt(3));

	let outBuf = new Uint8Array(memory.buffer, r, 32);
	let inBuf = new Uint8Array(memory.buffer, r + 32, 3);

	i.exports.bmt_hash_free(r);

	process.stdout.write(Buffer.from(outBuf).toString("hex") + "\n");
}

init();
